﻿namespace WinTaskOneAndTwo
{
    partial class Form1
    {
        /// <summary>
        ///  Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        ///  Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        ///  Required method for Designer support - do not modify
        ///  the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.txt_SurName = new System.Windows.Forms.TextBox();
            this.txt_Name = new System.Windows.Forms.TextBox();
            this.txt_MiddleName = new System.Windows.Forms.TextBox();
            this.nmc_Age = new System.Windows.Forms.NumericUpDown();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.listBox1 = new System.Windows.Forms.ListBox();
            this.btn_AddItem = new System.Windows.Forms.Button();
            this.btn_WriteToFile = new System.Windows.Forms.Button();
            this.btn_CleanList = new System.Windows.Forms.Button();
            this.btn_Edit = new System.Windows.Forms.Button();
            this.btn_ReadFromFile = new System.Windows.Forms.Button();
            this.btn_Sort = new System.Windows.Forms.Button();
            this.btn_Delete = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.nmc_Age)).BeginInit();
            this.SuspendLayout();
            // 
            // txt_SurName
            // 
            this.txt_SurName.Location = new System.Drawing.Point(70, 69);
            this.txt_SurName.Name = "txt_SurName";
            this.txt_SurName.Size = new System.Drawing.Size(194, 23);
            this.txt_SurName.TabIndex = 0;
            // 
            // txt_Name
            // 
            this.txt_Name.Location = new System.Drawing.Point(70, 98);
            this.txt_Name.Name = "txt_Name";
            this.txt_Name.Size = new System.Drawing.Size(194, 23);
            this.txt_Name.TabIndex = 1;
            // 
            // txt_MiddleName
            // 
            this.txt_MiddleName.Location = new System.Drawing.Point(70, 127);
            this.txt_MiddleName.Name = "txt_MiddleName";
            this.txt_MiddleName.Size = new System.Drawing.Size(194, 23);
            this.txt_MiddleName.TabIndex = 2;
            // 
            // nmc_Age
            // 
            this.nmc_Age.Location = new System.Drawing.Point(70, 156);
            this.nmc_Age.Maximum = new decimal(new int[] {
            150,
            0,
            0,
            0});
            this.nmc_Age.Name = "nmc_Age";
            this.nmc_Age.Size = new System.Drawing.Size(62, 23);
            this.nmc_Age.TabIndex = 3;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(26, 69);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(34, 15);
            this.label1.TabIndex = 4;
            this.label1.Text = "ФИО";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(26, 101);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(31, 15);
            this.label2.TabIndex = 5;
            this.label2.Text = "Имя";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(6, 130);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(58, 15);
            this.label3.TabIndex = 6;
            this.label3.Text = "Отчество";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(10, 158);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(50, 15);
            this.label4.TabIndex = 7;
            this.label4.Text = "Возраст";
            // 
            // listBox1
            // 
            this.listBox1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.listBox1.FormattingEnabled = true;
            this.listBox1.ItemHeight = 15;
            this.listBox1.Location = new System.Drawing.Point(270, 1);
            this.listBox1.Name = "listBox1";
            this.listBox1.Size = new System.Drawing.Size(532, 454);
            this.listBox1.TabIndex = 8;
            this.listBox1.SelectedIndexChanged += new System.EventHandler(this.listBox1_SelectedIndexChanged);
            // 
            // btn_AddItem
            // 
            this.btn_AddItem.Location = new System.Drawing.Point(70, 185);
            this.btn_AddItem.Name = "btn_AddItem";
            this.btn_AddItem.Size = new System.Drawing.Size(75, 23);
            this.btn_AddItem.TabIndex = 9;
            this.btn_AddItem.Text = "Добавить";
            this.btn_AddItem.UseVisualStyleBackColor = true;
            this.btn_AddItem.Click += new System.EventHandler(this.btn_AddItem_Click);
            // 
            // btn_WriteToFile
            // 
            this.btn_WriteToFile.Location = new System.Drawing.Point(42, 354);
            this.btn_WriteToFile.Name = "btn_WriteToFile";
            this.btn_WriteToFile.Size = new System.Drawing.Size(75, 54);
            this.btn_WriteToFile.TabIndex = 10;
            this.btn_WriteToFile.Text = "Записать в файл";
            this.btn_WriteToFile.UseVisualStyleBackColor = true;
            this.btn_WriteToFile.Click += new System.EventHandler(this.btn_WriteToFile_Click);
            // 
            // btn_CleanList
            // 
            this.btn_CleanList.Location = new System.Drawing.Point(70, 214);
            this.btn_CleanList.Name = "btn_CleanList";
            this.btn_CleanList.Size = new System.Drawing.Size(75, 23);
            this.btn_CleanList.TabIndex = 11;
            this.btn_CleanList.Text = "Очистить список";
            this.btn_CleanList.UseVisualStyleBackColor = true;
            this.btn_CleanList.Click += new System.EventHandler(this.btn_CleanList_Click);
            // 
            // btn_Edit
            // 
            this.btn_Edit.Location = new System.Drawing.Point(151, 185);
            this.btn_Edit.Name = "btn_Edit";
            this.btn_Edit.Size = new System.Drawing.Size(75, 23);
            this.btn_Edit.TabIndex = 12;
            this.btn_Edit.Text = "Редактиование";
            this.btn_Edit.UseVisualStyleBackColor = true;
            this.btn_Edit.Click += new System.EventHandler(this.btn_Edit_Click);
            // 
            // btn_ReadFromFile
            // 
            this.btn_ReadFromFile.Location = new System.Drawing.Point(134, 354);
            this.btn_ReadFromFile.Name = "btn_ReadFromFile";
            this.btn_ReadFromFile.Size = new System.Drawing.Size(75, 54);
            this.btn_ReadFromFile.TabIndex = 13;
            this.btn_ReadFromFile.Text = "Прочитать из файла";
            this.btn_ReadFromFile.UseVisualStyleBackColor = true;
            this.btn_ReadFromFile.Click += new System.EventHandler(this.btn_ReadFromFile_Click);
            // 
            // btn_Sort
            // 
            this.btn_Sort.Location = new System.Drawing.Point(51, 275);
            this.btn_Sort.Name = "btn_Sort";
            this.btn_Sort.Size = new System.Drawing.Size(94, 40);
            this.btn_Sort.TabIndex = 14;
            this.btn_Sort.Text = "Сортировка по возрасту";
            this.btn_Sort.UseVisualStyleBackColor = true;
            this.btn_Sort.Click += new System.EventHandler(this.btn_Sort_Click);
            // 
            // btn_Delete
            // 
            this.btn_Delete.Location = new System.Drawing.Point(151, 214);
            this.btn_Delete.Name = "btn_Delete";
            this.btn_Delete.Size = new System.Drawing.Size(75, 23);
            this.btn_Delete.TabIndex = 12;
            this.btn_Delete.Text = "Удаление";
            this.btn_Delete.UseVisualStyleBackColor = true;
            this.btn_Delete.Click += new System.EventHandler(this.btn_Delete_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.btn_Delete);
            this.Controls.Add(this.btn_Sort);
            this.Controls.Add(this.btn_ReadFromFile);
            this.Controls.Add(this.btn_Edit);
            this.Controls.Add(this.btn_CleanList);
            this.Controls.Add(this.btn_WriteToFile);
            this.Controls.Add(this.btn_AddItem);
            this.Controls.Add(this.listBox1);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.nmc_Age);
            this.Controls.Add(this.txt_MiddleName);
            this.Controls.Add(this.txt_Name);
            this.Controls.Add(this.txt_SurName);
            this.Name = "Form1";
            this.Text = "ФИО";
            this.Load += new System.EventHandler(this.Form1_Load);
            ((System.ComponentModel.ISupportInitialize)(this.nmc_Age)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox txt_SurName;
        private System.Windows.Forms.TextBox txt_Name;
        private System.Windows.Forms.TextBox txt_MiddleName;
        private System.Windows.Forms.NumericUpDown nmc_Age;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.ListBox listBox1;
        private System.Windows.Forms.Button btn_AddItem;
        private System.Windows.Forms.Button btn_WriteToFile;
        private System.Windows.Forms.Button btn_CleanList;
        private System.Windows.Forms.Button btn_Edit;
        private System.Windows.Forms.Button btn_ReadFromFile;
        private System.Windows.Forms.Button btn_Sort;
        private System.Windows.Forms.Button btn_Delete;
    }
}

