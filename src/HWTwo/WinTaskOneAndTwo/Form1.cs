﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.IO.Pipes;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using TaskOneAndTwo;
using TaskOneAndTwo.WorkWithFiles;

namespace WinTaskOneAndTwo
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }
        
        private void Form1_Load(object sender, EventArgs e)
        {
            initPersonDefault();
        }
        private void initPersonDefault()
        {
            listBox1.Items.Add(new Person { Name = "Семен", SurName = "Лобанов", MiddleName = "Петрович", Age = 40 });
            listBox1.Items.Add(new Person { Name = "Арина", SurName = "Мишустина", MiddleName = "Михаловна", Age = 27 });
            listBox1.Items.Add(new Person { Name = "Андрей", SurName = "Кузьмин", MiddleName = "Олегович", Age = 35 });
        }

        private void btn_WriteToFile_Click(object sender, EventArgs e)
        {
            using (SaveFileDialog openFileDialog = new SaveFileDialog())
            {
                openFileDialog.InitialDirectory = "c:\\";
                openFileDialog.Filter = "xml files (*.xml)|*.txt|json files (*.json)|*.json";
                openFileDialog.RestoreDirectory = true;
                openFileDialog.FilterIndex = 0;
                if (openFileDialog.ShowDialog() == DialogResult.OK)
                {
                    if (openFileDialog.FilterIndex == 1)
                    {
                        //openFileDialog.FileName += ".xml";
                        var file = new FileStream(openFileDialog.FileName, FileMode.OpenOrCreate);
                        using (var streamWriter = new OtusStreamWriter<List<Person>>(file,
                            listBox1.Items.Cast<Person>().ToList(), new OtusXmlSerializer<List<Person>>()))
                        {
                            streamWriter.Write();
                        }
                    }
                    else if (openFileDialog.FilterIndex == 2)
                    {
                        var file = new FileStream(openFileDialog.FileName, FileMode.OpenOrCreate);
                        using (var streamWriter = new OtusStreamWriter<List<Person>>(file,
                            listBox1.Items.Cast<Person>().ToList(), new OtusJsonSerializer<List<Person>>()))
                        {
                            streamWriter.Write();
                        }
                    }
                }
            }
        }

        private void btn_CleanList_Click(object sender, EventArgs e)
        {
            listBox1.Items.Clear();
        }

        private void btn_ReadFromFile_Click(object sender, EventArgs e)
        {
            using (OpenFileDialog openFileDialog = new OpenFileDialog())
            {
                openFileDialog.InitialDirectory = "c:\\";
                openFileDialog.Filter = "xml files (*.xml)|*.txt|json files (*.json)|*.json";
                openFileDialog.RestoreDirectory = true;

                if (openFileDialog.ShowDialog() == DialogResult.OK)
                {
                    if (openFileDialog.FilterIndex == 1)
                    {
                        var file = new FileStream(openFileDialog.FileName, FileMode.Open);
                        using (var streamReader =
                            new OtusStreamReader<Person>(file, new OtusXmlSerializer<Person>()))
                        {
                            for (int i = 0; i < streamReader.Count; i++)
                            {
                                listBox1.Items.Add(streamReader[i]);
                            }
                        }
                    }
                    else if (openFileDialog.FilterIndex == 2)
                    {
                        var file = new FileStream(openFileDialog.FileName, FileMode.Open);
                        using (var streamReader =
                            new OtusStreamReader<Person>(file, new OtusJsonSerializer<Person>()))
                        {
                            for (int i = 0; i < streamReader.Count; i++)
                            {
                                listBox1.Items.Add(streamReader[i]);
                            }

                        }
                    }
                }
            }
        }

        private void btn_Sort_Click(object sender, EventArgs e)
        {
            PersonSorter sorter = new PersonSorter();
            List<Person> persons = listBox1.Items.Cast<Person>().ToList().ToList();
            var lstPersonOrder = sorter.Sort<Person>(persons).ToList();
            listBox1.Items.Clear();
            for (int i = 0; i < lstPersonOrder.Count; i++)
            {
                listBox1.Items.Add(lstPersonOrder[i]);
            }
        }

        private void listBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            var person = (Person) listBox1.SelectedItem;
            SetPerson(person);
        }
        
        private void btn_AddItem_Click(object sender, EventArgs e)
        {
            listBox1.Items.Add(GetPerson(null));
        }
        
        private void btn_Edit_Click(object sender, EventArgs e)
        {
            var person = (Person)listBox1.SelectedItem;
            listBox1.Items[listBox1.SelectedIndex] = GetPerson(person, false);
        }

        private void btn_Delete_Click(object sender, EventArgs e)
        {
            listBox1.Items.Remove(listBox1.SelectedItem);
        }

        private Person GetPerson(Person person, bool isNew = true)
        {
            if (!isNew && person == null)
            {
                throw new ArgumentNullException("Не задан параметр человек");
            }
            else if (isNew && person == null)
                person = new Person();

            person.Age = (int)nmc_Age.Value;
            person.Name = txt_Name.Text;
            person.MiddleName = txt_MiddleName.Text;
            person.SurName = txt_SurName.Text;
            return person;
        }


        private void SetPerson(Person person)
        {
            if (person == null)
            {
                return;
            }

            nmc_Age.Value = person.Age;
            txt_Name.Text = person.Name;
            txt_MiddleName.Text = person.MiddleName;
            txt_SurName.Text = person.SurName;
        }

    }
}
