﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TaskThree.Rep.Cotract;

namespace TaskThree.Rep
{
    public class AccountRep : IRepository<Account>
    {
        private List<Account> accountList;

        public AccountRep()
        {
            accountList = new List<Account>();
            accountList.Add(new Account { FirstName = "Семен", LastName = "Семеныч", BirthDate = new DateTime(1961, 04, 12) });
            accountList.Add(new Account { FirstName = "Аркадий", LastName = "Николаевич", BirthDate = new DateTime(1965, 08, 12) });
            accountList.Add(new Account { FirstName = "Владимир", LastName = "Олегович", BirthDate = new DateTime(1975, 10, 12) });
        }
        public AccountRep(List<Account> list)
        {
            accountList = list;
        }
        
        public void Add(Account item)
        {
            accountList.Add(item);
        }

        public IEnumerable<Account> GetAll()
        {
            foreach (var item in accountList)
            {
                yield  return item;
            }
        }

        public Account GetOne(Func<Account, bool> predicate)
        {
            return accountList.FirstOrDefault(predicate);
        }
    }
}
