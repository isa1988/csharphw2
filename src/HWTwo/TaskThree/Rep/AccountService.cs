﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TaskThree.Rep.Cotract;

namespace TaskThree.Rep
{
    public class AccountService : IAccountService
    {
        private IRepository<Account> accountRep;
        public AccountService(IRepository<Account> rep)
        {
            accountRep = rep;
        }
        public void AddAccount(Account account)
        {
            if (string.IsNullOrWhiteSpace(account.FirstName))
                throw new NullReferenceException("Не заполненно имя");
            if (string.IsNullOrWhiteSpace(account.LastName))
                throw new NullReferenceException("Не заполненно второе имя");
            accountRep.Add(account);
        }

        public int CountTotal()
        {
            return accountRep.GetAll().Count();
        }
    }
}
