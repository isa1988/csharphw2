﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Xml;
using System.Xml.Linq;
using TaskOneAndTwo;
using System.Xml.Serialization;
using TaskOneAndTwo.WorkWithFiles.Contract;
using ExtendedXmlSerializer;
using ExtendedXmlSerializer.Configuration;
using Newtonsoft.Json;

namespace TaskOneAndTwo.WorkWithFiles
{
    public class OtusJsonSerializer<T> : ISerializer<T>
    {
        public T1 Deserialize<T1>(Stream stream)
        {
            using (StreamReader reader = new StreamReader(stream))
            using (JsonTextReader jsonReader = new JsonTextReader(reader))
            {
                JsonSerializer ser = new JsonSerializer();
                var retVal = ser.Deserialize<T1>(jsonReader);
                return retVal;
            }
        }

        public void Serialize<T1>(Stream stream, T1 item)
        {
            using (StreamWriter writer = new StreamWriter(stream))
            using (JsonTextWriter jsonWriter = new JsonTextWriter(writer))
            {
                JsonSerializer ser = new JsonSerializer();
                ser.Serialize(jsonWriter, item);
                jsonWriter.Flush();
            }
        }
    }
}
