﻿using Sprache;
using System;
using System.Collections.Generic;
using System.Text;

namespace TaskOneAndTwo.WorkWithFiles.Contract
{
    public interface ISorter<T>
    {
        IEnumerable<T> Sort<T>(IEnumerable<T> notSortedItems) where T : IPerson;
    }
}
