﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TaskOneAndTwo.WorkWithFiles.Contract
{
    public interface IPerson
    {
        int Age { get; set; }
    }
}
