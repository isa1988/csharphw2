﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace TaskOneAndTwo.WorkWithFiles.Contract
{
    public interface ISerializer<T>
    {
        void Serialize<T>(Stream stream, T item);
        T Deserialize<T>(Stream stream);
    }
}
