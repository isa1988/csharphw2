﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Xml;
using System.Xml.Linq;
using TaskOneAndTwo;
using System.Xml.Serialization;
using TaskOneAndTwo.WorkWithFiles.Contract;
using ExtendedXmlSerializer;
using ExtendedXmlSerializer.Configuration;

namespace TaskOneAndTwo.WorkWithFiles
{
    public class OtusXmlSerializer<T> : ISerializer<T>
    {
        public T1 Deserialize<T1>(Stream stream)
        {
            T1 arrayT;
            //string path = HttpContext.Current.ApplicationInstance.Server.MapPath("~/App_Data/") + "cars.xml";

            if (stream.Position > 0)
            {
                stream.Position = 0;
            }
            XmlRootAttribute xRoot = new XmlRootAttribute();
            xRoot.ElementName = "List";
            xRoot.IsNullable = true;
            XmlSerializer formatter = new XmlSerializer(typeof(T1), new XmlRootAttribute("List"));
            
            var retVal = (T1) formatter.Deserialize(stream);
            return retVal;
        }

        public void Serialize<T1>(Stream stream, T1 item)
        {
            XmlRootAttribute xRoot = new XmlRootAttribute();
            xRoot.ElementName = "List";
            xRoot.IsNullable = true;
            XmlSerializer formatter = new XmlSerializer(typeof(T1), xRoot);
            formatter.Serialize(stream, item);

        }
    }
}
