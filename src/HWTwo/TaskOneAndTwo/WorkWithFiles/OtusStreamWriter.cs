﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using TaskOneAndTwo.WorkWithFiles.Contract;

namespace TaskOneAndTwo.WorkWithFiles
{
    public class OtusStreamWriter<T> : IDisposable
    {
        private Stream stream;
        private T lstValue;
        private ISerializer<T> serializer;
        public OtusStreamWriter(Stream stream, T lstValue, ISerializer<T> serializer)
        {
            this.stream = stream;
            this.lstValue = lstValue;
            this.serializer = serializer;
        }

        public void Write()
        {
            serializer.Serialize<T>(stream, lstValue);
        }

        public void Dispose()
        {
            if (stream != null)
                stream.Dispose();
        }
    }
}
