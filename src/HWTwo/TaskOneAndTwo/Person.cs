﻿using System;
using TaskOneAndTwo.WorkWithFiles.Contract;

namespace TaskOneAndTwo
{
    public class Person : IPerson
    {
        public string Name { get; set; }
        public string MiddleName { get; set; }
        public string SurName { get; set; }
        public int Age { get; set; }

        public override string ToString()
        {
            return $"{SurName} {Name} {MiddleName} {Age}";
        }
    }
}
