﻿using ExtendedXmlSerializer.Core.Sources;
using Sprache;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Text;
using TaskOneAndTwo.WorkWithFiles.Contract;

namespace TaskOneAndTwo
{
    public class PersonSorter : ISorter<Person>
    {
        public IEnumerable<T> Sort<T>(IEnumerable<T> notSortedItems)  where T : IPerson
        {
            List<T> persons = (List<T>)notSortedItems;
            T tmp;
            //расстояние между элементами, которые сравниваются
            var d = persons.Count / 2;
            while (d >= 1)
            {
                for (var i = d; i < persons.Count; i++)
                {
                    var j = i;
                    while ((j >= d) && (persons[j - d].Age > persons[j].Age))
                    {
                        Swap<T>(persons,  j, j - d);
                        j = j - d;
                    }
                }

                d = d / 2;
            }

            return (IEnumerable<T>)persons;
        }

        void Swap<T>(List<T> lst, int i, int j)
        {
            var t = lst[i];
            lst[i] = lst[j];
            lst[j] = t;
        }
    }
}
