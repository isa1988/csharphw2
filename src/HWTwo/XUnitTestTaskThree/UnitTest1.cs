using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using TaskThree;
using TaskThree.Rep;
using TaskThree.Rep.Cotract;
using Xunit;

namespace XUnitTestTaskThree
{
    public class UnitTest1
    {
        [Fact]
        public void GetAll()
        {
            var accountRep = new AccountRep(GetData().ToList());

            var response = accountRep.GetAll();
   
            Assert.Equal(response.Count(), 3);
        }
        [Fact]
        public void GetOne()
        {
            var accountRep = new AccountRep(GetData().ToList());

            var response = accountRep.GetOne(n => n.FirstName == "�����");
            var responseIsNull = accountRep.GetOne(n => n.FirstName == "�����1");

            Assert.Null(responseIsNull);
            Assert.NotNull(response);
            Assert.Equal(response.FirstName, "�����");
            Assert.Equal(response.LastName, "�������");
            Assert.Equal(response.BirthDate, new DateTime(1961, 04, 12));
        }
        [Fact]
        public void Add()
        {

            var accountRep = new AccountRep(GetData().ToList());

            var response = accountRep.GetAll();
            Assert.Equal(response.Count(), 3);
            accountRep.Add(new Account{FirstName = "����", LastName = "��������", BirthDate = new DateTime(2000, 10, 20)});

            response = accountRep.GetAll();
            Assert.Equal(response.Count(), 4);
        }


        [Fact]
        public void AddService()
        {
            var m = new Mock<AccountRep>();

            var accountSer = new AccountService(m.Object);

            Assert.Equal(accountSer.CountTotal(), 3);
            try
            {
                accountSer.AddAccount(new Account { FirstName = "", LastName = "��������", BirthDate = new DateTime(2000, 10, 20) });
            }
            catch (Exception ex)
            {
                Assert.Equal(ex.Message, "�� ���������� ���");
            }

            Assert.Equal(accountSer.CountTotal(), 3);
            try
            {
                accountSer.AddAccount(new Account { FirstName = "����", LastName = "", BirthDate = new DateTime(2000, 10, 20) });
            }
            catch (Exception ex)
            {
                Assert.Equal(ex.Message, "�� ���������� ������ ���");
            }

            Assert.Equal(accountSer.CountTotal(), 3);
            
            accountSer.AddAccount(new Account { FirstName = "����", LastName = "��������", BirthDate = new DateTime(2000, 10, 20) });

            Assert.Equal(accountSer.CountTotal(), 4);
        }
        private IEnumerable<Account> GetData()
        {
            return new List<Account>
            {
                new Account {FirstName = "�����", LastName = "�������", BirthDate = new DateTime(1961, 04, 12)},
                new Account {FirstName = "�������", LastName = "����������", BirthDate = new DateTime(1965, 08, 12)},
                new Account {FirstName = "��������", LastName = "��������", BirthDate = new DateTime(1975, 10, 12)}
            };
        }
    }
}
